#!/usr/bin/env bash

set -euo pipefail
set -x

script_dir=$(dirname $(readlink -f $0))

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

checkout()
{
    if [ -d libreoffice ]; then
        return
    fi
    git clone --depth 1 --branch $version https://github.com/LibreOffice/core libreoffice
    pushd libreoffice
    git log -n1

    # fix makefile
    # $(INCLUDE) contains path to windows sdk, with spaces, when calling midl.exe
    # It creates an error. Removing INCLUDE seems to resolve the problem.
    sed -e '/INCLUDE/d' -i shell/CustomTarget_spsupp_idl.mk

    # set shallow submodules
    git config -f .gitmodules submodule.translations.shallow true
    git config -f .gitmodules submodule.helpcontent2.shallow true
    git config -f .gitmodules submodule.dictionaries.shallow true
    git submodule init
    git submodule update --progress
    popd
}

cygwin_path=$(pwd)/cygwin

get_cygwin()
{
    # https://wiki.documentfoundation.org/Development/BuildingOnWindows
    if [ -f cygwin/installed ]; then
        return
    fi
    rm -rf cygwin
    if [ ! -f setup-x86_64.exe ]; then
        wget https://www.cygwin.com/setup-x86_64.exe
    fi

    # https://www.cygwin.com/faq.html#faq.setup.cli
    ./setup-x86_64.exe \
        --no-desktop --no-shortcuts --no-admin --wait \
        --quiet-mode --root $(cygpath -m $cygwin_path) \
        --site http://cygwin.org/pub/cygwin/ \
        -P autoconf -P automake -P bison -P cabextract -P doxygen -P flex \
        -P gcc-g++ -P gettext-devel -P git -P gnupg -P gperf -P make -P mintty \
        -P nasm -P openssh -P openssl -P patch -P perl -P python -P python3 \
        -P pkg-config -P rsync -P unzip -P vim -P wget -P zip\
        -P perl-Archive-Zip -P perl-Font-TTF -P perl-IO-String

    # replace cygwin make with libreoffice version
    rm cygwin/bin/make.exe
    wget https://dev-www.libreoffice.org/bin/cygwin/make-4.2.1-msvc.exe
    cp make-4.2.1-msvc.exe cygwin/bin/make.exe
    chmod +x cygwin/bin/make.exe
    mkdir -p cygwin/opt/lo/bin
    cp -r cygwin/bin/make.exe cygwin/opt/lo/bin

    touch cygwin/installed
}

build()
{
    pushd libreoffice
    cmd_dir=$(dirname $(which cmd.exe))
    configure_script=$(readlink -f configure.sh)
    build_script=$(readlink -f build.sh)
    if [ ! -f configured ]; then
        cat > configure.sh << EOF
set -x
set -euo pipefail
cd /cygdrive/$(pwd)
# set path to java
export PATH=\$PATH:$(dirname $(which java))
# args for configure taken from daily build
# https://ci.libreoffice.org/job/lo_daily_tb_win_arm64
./autogen.sh \
 --host=aarch64-pc-cygwin \
 --disable-pch --disable-ccache \
 --disable-dependency-tracking --disable-online-update \
 --without-junit --without-helppack-integration \
 --with-java=no
EOF
        PATH=$cygwin_path/bin:$cmd_dir cmd.exe /c "bash.exe --login /cygdrive/$configure_script"
        touch configured
    fi

    cat > build.sh << EOF
set -x
set -euo pipefail
cd /cygdrive/$(pwd)
make -j$(nproc)
EOF
    PATH=$cygwin_path/bin:$cmd_dir cmd.exe /c "bash.exe --login /cygdrive/$build_script"
    popd
}

fix_vs()
{
    # LibreOffice uses devinit.exe. For an unknown reason, it's not installed
    # with VS2022. Just copy from 2019.
    rsync -av \
    "/c/Program Files (x86)/Microsoft Visual Studio/2019/Community/Common7/Tools/devinit/" \
    "/c/Program Files/Microsoft Visual Studio/2022/Community/Common7/Tools/devinit/"
}

checkout
get_cygwin
fix_vs
build
